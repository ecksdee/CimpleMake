#=================================CimpleMake===================================#
#             This is a generic makefile for simple C/C++ projects             #
#               Change the variables below to suit your needs...               #
# ---------------------------------------------------------------------------- #
#   Copyright (C) 2016-2017 Xavier Dupont - All Rights Reserved                #
#   You may use, distribute and modify this code under the terms of the        #
#   2-Clause BSD license.                                                      #
#                                                                              #
#   You should have received a copy of the 2-Clause BSD license with           #
#   this file. If not, please write to my email below, or visit:               #
#   https://opensource.org/licenses/BSD-2-Clause                               #
# ---------------------------------------------------------------------------- #
# Available rules:                                                             #
# 	[nothing]: executes the default rule                                       #
# 	default  : executes the release rule                                       #
# 	release  : compiles and link SOURCE files into the RLS_BIN                 #
# 	debug    : executes the release rule with additional flags (DBGFLAGS)      #
# 	prof     : executes the release rule with profiling flags needed by gprof  #
#                                                                              #
# 	clean    : deletes object and binary files (not directories)               #
# 	archive  : archives TO_ARCHIVE into a tarball called ARCHIVE_NAME.tar.gz   #
#   run      : runs the release rule with arguments given in ARGS              #
# 	valgrind : executes the debug rule and calls valgrind on its result        #
# ---------------------------------------------------------------------------- #
# Created by : Xavier Dupont                                                   #
# Mail       : 3cksDee@gmail.com                                               #
# URL        : https://gitlab.com/EcksDee/CimpleMake                           #
# Version    : 0.8                                                             #
#------------------------------------------------------------------------------#

# Compiling and linking variables
# -------------------------------

# CC           The C/C++ compiler
# LD           The C/C++ linker
CC             := #gcc
LD             := #gcc

# CFLAGS       Compilation flags
# LDFLAGS      Linking flags
# DBGFLAGS     Debug flags
# LIBS         Libraries to add when linking
CFLAGS         := #--std=c99 --pedantic -Wall -Wextra -DNDEBUG
LDFLAGS        :=
DBGFLAGS       := #-g
LIBS           := #-lm

# ! *_DIR variables must not end with '/' or trailing spaces !

# SOURCE_DIR   The directory where .c and .h files are located
# SOURCE       Source files to be compiled and linked, order might be important!
#              You may not include header files in the SOURCE variable.
SOURCE_DIR     := .
SOURCE         := #main.c

# SRC_EXT      The extension of the source files (typically '.c' or '.cpp')
# HDR_EXT      The extension of the header files (typically '.h' or '.hpp')
SRC_EXT        := #.c
HDR_EXT        := #.h

# MISSING_H    Is there to prevent any unwanted dependency (like nonexistent
#              headers for example) being added when compiling sources
MISSING_H      := #main.h

# OBJ_DIR      The directory where the object files will end up
# BIN_DIR      The directory where binary files will end up
OBJ_DIR        := .
BIN_DIR        := .

# RLS_BIN      Name of the main binary
# DBG_BIN      Name of the debug binary
# PRF_BIN      Name of the profiling binary, this is to be used with gprof
#              Note that you may not name any of these 'default' unless you
#              declared a rule above the generic warning
RLS_BIN        := main
DBG_BIN        := debug
PRF_BIN        := prof

# Tool variables
# --------------

# ARCHIVE_NAME Name of the archive, without its extension
# TO_ARCHIVE   Files or directories to be archived. You can use '$(DSOURCE)' to
#              add your source files. Warning: using $(DSOURCE) will not include
#              headers that do not have a source file associated with them and
#              that are not in the SOURCE variable
ARCHIVE_NAME   := archive
TO_ARCHIVE      = #$(DSOURCE)

# ARGS         Arguments to pass when using 'run' and 'valgrind' rules
ARGS           := #-h

# VALGRIND_OPT Options to pass to valgrind when calling the valgrind rule
VALGRIND_OPT   := #--leak-check=full --track-origins=yes

# !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!
# !    Everything below this line should be generic and thus left untouched    !
# !++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!

# Disable built-ins
MAKEFLAGS += --no-builtin-rules
.SUFFIXES:#

# Check that necessary variables were set

ifeq ($(and $(CC), $(LD)),)
  $(error Either the compiler or the linker variable was not set)
endif

ifeq ($(SOURCE),)
  $(error The SOURCE variable was not set)
endif

ifeq ($(and $(SRC_EXT), $(HDR_EXT)),)
  $(error Either the source extension or header extension variable was not set)
endif

ifeq ($(and $(SOURCE_DIR), $(OBJ_DIR), $(BIN_DIR)),)
  $(error At least one of the 'DIR' variables was not set. To set to current directory, set it to '.')
endif

ifeq ($(and $(RLS_BIN), $(DBG_BIN), $(PRF_BIN)),)
  $(error At least one of the 'BIN' variables was not set)
endif

RLS_TRGT := $(BIN_DIR)/$(RLS_BIN)
DBG_TRGT := $(BIN_DIR)/$(DBG_BIN)
PRF_TRGT := $(BIN_DIR)/$(PRF_BIN)

BIN_LIST := $(RLS_TRGT) $(DBG_TRGT) $(PRF_TRGT)

.PHONY: default
default: $(RLS_TRGT)

# The following prevents circular dependencies when the user provides binary
# file names equal to the corresponding target name. Also, do not declare the
# binaries as phony if their name is the same as their target
ifneq (./release, $(RLS_TRGT))
.PHONY: release
release:: $(RLS_TRGT)
endif

ifneq (./debug, $(DBG_TRGT))
.PHONY: debug
debug:: $(DBG_TRGT)
endif

ifneq (./prof, $(PRF_TRGT))
.PHONY: prof
debug:: $(PRF_TRGT)
endif

OBJ        := $(SOURCE:$(SRC_EXT)=.o)
DOBJ       := $(addprefix $(OBJ_DIR)/, $(OBJ))
DMISSING   := $(addprefix $(SOURCE_DIR)/, $(MISSING_H))

$(DBG_TRGT):: CFLAGS := $(filter-out -DNDEBUG, $(CFLAGS))
$(DBG_TRGT):: CFLAGS += $(DBGFLAGS)
$(DBG_TRGT):: clear_objects

$(PRF_TRGT):: CFLAGS  += -pg
$(PRF_TRGT):: LDFLAGS += -pg
$(PRF_TRGT):: clear_objects

# ---------------------------------- LINKING --------------------------------- #

$(BIN_LIST):: $(DOBJ)
	@mkdir -v -p $(BIN_DIR)
	@$(LD) $^ -o $@ $(LDFLAGS) $(LIBS)
	@echo Created \'$(notdir $@)\' in \'$(BIN_DIR)/\' successfully!

# --------------------------------- COMPILING -------------------------------- #

.SECONDEXPANSION:
$(DOBJ): %.o: $$(filter-out $$(DMISSING), $$(addprefix $$(SOURCE_DIR)/, $$(notdir $$*$(SRC_EXT) $$*$(HDR_EXT))))
	@mkdir -v -p $(OBJ_DIR)
	@$(CC) -c $< -o $@ $(CFLAGS)

# ----------------------------------- TOOLS ---------------------------------- #

.PHONY: clean archive run valgrind clear_objects

DSOURCE = $(patsubst ./%,%,$(addprefix $(SOURCE_DIR)/, $(SOURCE)))             \
$(patsubst ./%,%,$(addprefix $(SOURCE_DIR)/, $(filter-out $(MISSING_H),$(SOURCE:$(SRC_EXT)=$(HDR_EXT)))))

clean:
	@rm -f $(DOBJ) $(ARCHIVE_NAME).tar.gz $(BIN_LIST)

archive:
ifeq ($(ARCHIVE_NAME),)
	$(error ARCHIVE_NAME is empty)
endif
ifeq ($(TO_ARCHIVE),)
	$(error TO_ARCHIVE variable is empty)
endif
	@tar -zcvf $(ARCHIVE_NAME).tar.gz $(TO_ARCHIVE)

run: $(RLS_TRGT)
	@./$(RLS_TRGT) $(ARGS)

valgrind: $(DBG_TRGT)
	@valgrind $(VALGRIND_OPT) $(DBG_TRGT) $(ARGS)

clear_objects:
	@rm -f $(DOBJ)
	@echo \(Re\)building objects with additional flags...
