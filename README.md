# CimpleMake
## Introduction
**CimpleMake** is a makefile made to simplify the compilation of *simple* C/C++ projects as well as common tasks related to such projects.

## What you can and cannot do with CimpleMake
You can:
* create a binary from source files as long as they are in the *SOURCE_DIR* directory.
* specify flags for compiling as well as linking and debugging.
* profile your application with the help of the *prof* rule, which uses gprof.
* call valgrind with predefined options on the debug version

You cannot:
* create a binary from source files that are in different folders.

## Available rules
* [nothing]: executes the default rule
* default: compiles and link SOURCE files into the MAIN_BIN
* debug: executes the default rule with additional flags (DBGFLAGS)
* prof: executes the default rule with profiling flags needed by gprof
* clean: deletes object and binary files (not directories)
* archive: archives TO_ARCHIVE into a tarball called ARCHIVE_NAME.tar.gz
* valgrind: executes the debug rule and calls valgrind on its result

## How it works
There are predefined variables that you must fill for the makefile to work.
These variables are then used to execute common procedures on them.

## Important note
Headers are automatically added to the compilation dependency unless they are specified in the MISSING_H variable.

## Requirements
You MUST have GNU make installed for this makefile to work, any other make won't.

## Known issues:
- Fails when sources have a directory prefix (probably won't fix)
- No support for source files with mixed extensions (might fix if needed)
- The run rule says it fails whenever the run program returned an error value (ie. different than zero)
